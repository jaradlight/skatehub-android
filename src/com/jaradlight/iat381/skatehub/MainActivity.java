package com.jaradlight.iat381.skatehub;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.jaradlight.iat381.skatehub.R;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{

	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	public static final int MEDIA_TYPE_IMAGE = 1;
	Button takePhoto;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        takePhoto = (Button) findViewById(R.id.takePhoto);
        takePhoto.setOnClickListener(this);
    }
    
 // THIS NEEDS TO BE KEPT
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
    
    @Override
	public void onClick(View v) {
		Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); // create Intent to take a picture and return control to the calling application
		//Uri uriSavedImage = Uri.fromFile(getOutputMediaFile(1));
		//Uri uriSavedImage = Uri.fromFile(new File("/sdcard/skateLocImage.png")); // create a file to save the image
		File savedImage = getOutputMediaFile(MEDIA_TYPE_IMAGE);
		i.putExtra(MediaStore.EXTRA_OUTPUT, savedImage); // set the image file name
		startActivityForResult(i, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE); //start the image capture intent
	}
    
    // KEEP THIS PLEASE
	public void onClickCreateView(MenuItem item) // Must use Menuitem because that is the clicked item
	{
		Log.d("MenuItem", "onClickCreateView :: "+item.getItemId());
		Intent i = new Intent(this, CreateActivity.class);
		startActivity(i);
	}
	
	// KEEP THIS PLEASE
	public void onClickFeedView(View v)
	{ // Handles the click of the "View Feed" button.
		Intent i = new Intent(this, FeedActivity.class);
		startActivity(i);
	}

    private static Uri getOutputMediaFileUri(int type) {
    	return Uri.fromFile(getOutputMediaFile(type));
	}
    
    private static File getOutputMediaFile(int type) {
    	File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
    			Environment.DIRECTORY_PICTURES), "MyCameraApp");
    	if (! mediaStorageDir.exists()) {
    		if (! mediaStorageDir.mkdirs()) {
    			Log.d("MyCameraApp", "failed to create directory");
    			return null;
    		}
    	}
    	
    	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    	File mediaFile;
    	if (type == MEDIA_TYPE_IMAGE) {
    		mediaFile = new File(mediaStorageDir.getPath() + File.separator +
    				"IMG_" + timeStamp + ".jpg");
    	} else {
    		return null;
    	}
    	return mediaFile;
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
    		if (resultCode == RESULT_OK) {

        		//image captured and saved to fileUri specified in the Intent
        		Toast.makeText(this, "Image saved to:\n" + 
        				data.getData(), Toast.LENGTH_LONG).show();
        	} else if (resultCode == RESULT_CANCELED) {
        		Toast.makeText(this, "The image capture was cancelled by the user", Toast.LENGTH_LONG).show();
        	} else {
        		Toast.makeText(this, "The image capture was failed", Toast.LENGTH_LONG).show();
        	}
    	}    	
    }
}

