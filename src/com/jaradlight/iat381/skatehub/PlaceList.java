package com.jaradlight.iat381.skatehub;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class PlaceList {
	
	// This will be where we handle saving and stuff.
	// Places will be stored in an ArrayList in here.

	Context context;
	private ArrayList <Place> placesList;
	
	// The name of the file where we store the data.
	private final String fileName = "locationData.json";
	
	
	public PlaceList (Context context){
		this.context = context;
		placesList = new ArrayList<Place>();
	}
	
	
	public void addPlace(Place place){
		// Adds a given place to the top of the list.
		placesList.add(0, place);
	}
	
	public int getSize(){
		return placesList.size();
	}
	
	
	public Place getPlace(int index){
		return placesList.get(index);
	}
	

	public void saveToFile(){
		FileOutputStream outputStream;
		
		JSONObject o = this.toJSONObject();
		String jsonData = o.toString(); 
		
		try {
		  outputStream = context.openFileOutput(this.fileName, Context.MODE_PRIVATE);
		  outputStream.write(jsonData.getBytes());
		  outputStream.close();
		} catch (Exception e) {
		  e.printStackTrace();
		}
	}
	
	
	public JSONObject toJSONObject(){
		JSONObject jsonObject = new JSONObject();
				
		for(int i=0; i<placesList.size(); i++)
		{
			JSONObject p = placesList.get(i).toJSONObject();
			try {
				jsonObject.accumulate(Integer.toString(i),p);
			} catch (Exception e){}
		}
		 
		try {
			return jsonObject;
		} catch (Exception e){
			Log.e("json","something broke up with json.");
			return null;
		}
	}
	
	
	public void loadFromFile(){
	    String fileString = this.getFile();
		if(fileString != null)
		{
			try {
				this.populateFromJSON(fileString,true);
			} catch (Exception e) {
				Log.e("error", "Error setting up placesList from file.");
				Log.e("error", fileString);
			}
		}
	}
	
	
	private String getFile()
	{
		//Find the directory for the SD Card using the API
		//*Don't* hardcode "/sdcard"
		File sdcard = context.getFilesDir();

		//Get the text file
		File file = new File(sdcard,this.fileName);

		//Read text from file
		StringBuilder text = new StringBuilder();

		try {
		    BufferedReader br = new BufferedReader(new FileReader(file));
		    String line;

		    while ((line = br.readLine()) != null) {
		        text.append(line);
		        text.append('\n');
		    }
		    br.close();
		    return text.toString();
		}
		catch (IOException e) {
		    //You'll need to add proper error handling here
			Log.e("error","Error reading local JSON file.");
		}
		return null;
	}
	
	
	public void populateFromJSON(String json, boolean clearList) throws JSONException  // setup places from JSON string
	{
		
		JSONArray object = new JSONArray(json);// read JSON string
		//JSONArray placesArray = object.getJSONArray("places"); // select the Places section
		
		for(int i=0; i < object.length() ;i++){ // loop thru places
			JSONObject JSONPlace = (JSONObject)object.get(i); // grab each place
			Place p = new Place(context, JSONPlace); // create place
			placesList.add(p); // add place to list
		}
	}


	public void clear() {
		// reset the list
		placesList.clear();		
	}
	
}
