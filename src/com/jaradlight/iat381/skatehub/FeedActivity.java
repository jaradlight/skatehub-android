package com.jaradlight.iat381.skatehub;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.jaradlight.iat381.skatehub.R;
import com.loopj.android.image.SmartImageView;

public class FeedActivity extends Activity implements OnClickListener{
	
	// This Activity will list all the points that are stored
	// locally and eventually the server if we get that far.
	
	PlaceList placeList;
	SkatehubRestClientUsage client;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feed);
		
		// Set up PlaceList object
		this.placeList = new PlaceList(getApplicationContext());
		
		// Create RestClient for later HTTP requests.
		client = new SkatehubRestClientUsage(this);
		
		
		
		getDataFromServer();
		checkForCreatedPlace();		
		
		//loadLayout();
	
	}
	
	private void checkForCreatedPlace(){
		
		Log.d("feed","Check For Created Place.");
		
		// Checks the intent for a new place form the CreateActivity
		// Creates a new Place object and adds it to the list.
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		if(extras != null){
			
			// Name
			String name = extras.getString("name");
			
			//Image File
			String imagePath = extras.getString("imagePath");
			File imageFile = new File(imagePath);
			
			// LatLonPoint
			double latitude = extras.getDouble("latitude");
			double longitude = extras.getDouble("longitude");
			LatLonPoint point = new LatLonPoint(latitude,longitude); 
			
			
			Place newPlace = new Place(getApplicationContext(),name, point, imageFile);
			placeList.addPlace(newPlace);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.feed, menu);
		return true;
	}

	private void loadLayout()
	{
		Log.d("feed","Load Layout.");
		
		LinearLayout linearLayout = (LinearLayout)findViewById(R.id.feedView);
		//linearLayout.removeAllViews();
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if( placeList.getSize() > 0){
			for(int i=0; i<placeList.getSize(); i++){
				Place p = placeList.getPlace(i);
				
				RelativeLayout parent = (RelativeLayout) inflater.inflate(R.layout.feed_place_item, null);
				
				
				// Set up Place title.
				TextView titleText = (TextView) parent.findViewById(R.id.placeTitle);
				titleText.setText(p.getName());
				titleText.setTextSize(26);
				titleText.setTextColor(Color.BLACK);
				
				// Set up image
				//ImageView img = (ImageView) parent.findViewById(R.id.testImage);
				SmartImageView img = (SmartImageView) parent.findViewById(R.id.testImage);
				img.setImageUrl(p.getServerImageUrl());
				
				// If the Place has an image
//				if(p.hasImage()){
//					Log.d("placeimage",p.getName() + "HAS AN IMAGE");
//					
//					Bitmap bmp = getImageFromFile(p.getImagePath());
//					img.setImageBitmap(bmp);
//					
//				}
//				// If the Place doesn't have an image.
//				else {
//					Log.d("placeimage",p.getName() + " doesn't have an image.");
//					
//					Resources res = getResources();
//					//img.setPadding(25, 25, 25, 25);
//					img.setImageDrawable(res.getDrawable(R.drawable.ic_launcher));
//				}
				
//				SmartImageView siv = new SmartImageView(getApplicationContext());
//				siv.setImageUrl(p.getServerImageUrl());
//				
//				linearLayout.addView(siv);
				
				// Set this activity as the click listener so we can
				// click this view and to go the Single Location activity
				parent.setOnClickListener(this);
				
				// Attach the Place object to the view,
				//so we have the details for later
				//parent.setTag(0, p);
				parent.setTag(p);
				
				// Add the parent view to the layout.
				linearLayout.addView(parent);
			}
		} else {
			TextView tv = new TextView(getApplicationContext());
			tv.setText("Server returned no locations!");
			tv.setBackgroundColor(Color.rgb(188, 71, 67));
			tv.setTextColor(Color.rgb(232, 185, 181));
			tv.setTextSize(24.0f);
			tv.setPadding(0, 15, 0, 15);
			tv.setGravity(Gravity.CENTER);
			linearLayout.addView(tv);
		}
		
		ScrollView scrollView = (ScrollView) linearLayout.getParent();
		scrollView.fullScroll(ScrollView.FOCUS_UP);
	}
	
	private void clearLayout(){
		// This clears the layout of child items so we can
		// have it ready to fresh data.
		
		Log.d("feed","Clear Layout.");
		
		LinearLayout linearLayout = (LinearLayout)findViewById(R.id.feedView);
		linearLayout.removeAllViews();
		
		
	}
	
	private Bitmap getImageFromFile(String path){
		return BitmapFactory.decodeFile(path);
	}

	
	public void updatePlaceList(String json){
		Log.d("feed","Update Place List");
		try {
			placeList.clear();
			placeList.populateFromJSON(json, true);
		} catch (Exception e) {
			Log.e("error", e.getMessage());
			Toast.makeText(getApplicationContext(), "Refresh failed.", Toast.LENGTH_SHORT).show();
		}
		
		loadLayout();
	}
	
	@Override
	public void onClick(View v) {
		
		// Get the Place object stored in the view that was clicked
		Place place = (Place)v.getTag();
		
		// Debug messages
		// Log.d("click", v.toString());
		//Toast.makeText(getApplicationContext(), place.getName(), Toast.LENGTH_SHORT).show();
		
		// Create the intent
		Intent intent = new Intent(this, SingleActivity.class);
		
		// Add the data for the place we're about to view
		intent.putExtra("name",place.getName());
		intent.putExtra("imagePath",place.getServerImageUrl());
		intent.putExtra("latitude",place.getLat());
		intent.putExtra("longitude",place.getLon());
		
		// Start the intent
		startActivity(intent);

//		Intent i = new Intent(this, SingleActivity.class);
//		startActivity(i);
		
	}
	
//	public void onClickFeedSave(MenuItem item) // Must use Menuitem because that is the clicked item
//	{
//		placeList.saveToFile();
//		
//		LinearLayout linearLayout = (LinearLayout)findViewById(R.id.feedView);
//		linearLayout.removeAllViews();
//		
//		placeList.loadFromFile();
//		
//		//refreshView();
//	}
	
	public void onClickFeedRefresh(MenuItem item) // Must use Menuitem because that is the clicked item
	{
		Log.d("MenuItem", "onClickFeedRefresh :: "+item.getItemId());
		Log.d("feed","On Click Feed Refresh");
		clearLayout();
		getDataFromServer();
		//refreshView();
	}
	
	public void onClickFeedAdd(MenuItem item) // Must use Menuitem because that is the clicked item
	{
		Log.d("MenuItem", "onClickFeedAdd :: "+item.getItemId());
		Log.d("feed","On Click Feed Add");
		//clearLayout();
		//getDataFromServer();
		//refreshView();
		Intent i = new Intent(this, CreateActivity.class);
		startActivity(i);
	}
	
//	public void onClickFeedLoadFile(MenuItem item) // Must use Menuitem because that is the clicked item
//	{
//		Log.d("MenuItem", "onClickFeedLoadFIle :: "+item.getItemId());
//		//getDataFromServer();
//		getDataFromFile();
//		//refreshView();
//	}
	
//	private void getDataFromFile() {
//		// TODO Auto-generated method stub
//		Toast.makeText(getApplicationContext(), "File loading not implemented.", Toast.LENGTH_SHORT).show();
//	}

	private void getDataFromServer(){
		Log.d("feed","BEGIN Get Data From Server.");
		// gets the most recent data from the server.
		client = new SkatehubRestClientUsage(this);
		try {
			client.getAllLocations();
			Log.d("places","Places: "+Integer.toString(this.placeList.getSize()));
		} catch(Exception e) {
			Toast.makeText(getApplicationContext(), "Request failed.", Toast.LENGTH_SHORT).show();
		}
		Log.d("feed","END Get Data From Server.");
	}
	
	
	// starts the intent to call Detailed Info Activity
	public void onClickLocInfo(MenuItem item) // Must use Menuitem because that is the clicked item
	{
		Intent i = new Intent(this, LocInfo.class);
		startActivity(i);
	}
	
}
