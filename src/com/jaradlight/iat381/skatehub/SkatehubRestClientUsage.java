package com.jaradlight.iat381.skatehub;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class SkatehubRestClientUsage {
	// A class to manage queries
	
	JsonHttpResponseHandler rHandler;
	FeedActivity f;
	
	public SkatehubRestClientUsage(FeedActivity f){
		this.f = f;
	}
	public SkatehubRestClientUsage(){
		
	}
	
	public void getAllLocations() throws JSONException {
        SkatehubRestClient.get("all", null, rHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(JSONArray response) {
                Log.d("http",response.toString());
                f.updatePlaceList(response.toString());
            }
        });
	}
	
	public void postNewPlace(Place place){
		// POST the place object to the server.
		
		Map<String, String> placeMap = place.getMap(); // Get attributes we want to post	
						
		
		// grab the regular parameters
		RequestParams requestParams = new RequestParams(placeMap);
		
		// Add the image file as well.
		if(place.hasImage()){
			Log.d("savedata","This place has an image so im gonna upload it!");
			File myFile = new File(place.getImagePath());
			try {
				Log.d("savedata","imagepath to RequstParams: "+myFile.toString());
			    requestParams.put("photo", myFile);
			} catch(FileNotFoundException e) {}
		} else {
			Log.d("image","This place doesnt have an image. thats not good.");
		}
		
		
		// POST to the server
		SkatehubRestClient.post("add", requestParams, rHandler);
	}

	
	
}

