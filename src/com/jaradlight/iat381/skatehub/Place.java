package com.jaradlight.iat381.skatehub;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.Toast;

public class Place {
	
	// This is a very basic setup for the app locations.
	// We should organize this soon so we can work on
	// different aspects of the app while knowing that
	// this is common.
	private Context context;
	private String name;
	private File image;
	private LatLonPoint point;
	private String serverImageUrl;
	
	public Place(Context c){
		this.context = c;
		this.name = "Random Place #"+Long.toString(Math.round(Math.random() * 100));
		this.point = new LatLonPoint(-14, 32);
		//this.photo = "non/existant/path/to/text.jpg";		
	}
	
	public Place(Context c, String name, LatLonPoint point){
		this.context = c;
		this.name = name;
		this.point = point;
		this.image = null;
	}
	
	public Place(Context c,String name, LatLonPoint point, File file){
		this.context = c;
		this.name = name;
		this.point = point;
		this.image = file;
	}
	
	// Create from a JSON object.
	public Place(Context c, JSONObject object) throws JSONException {
		this.name = object.getString("name"); // get name from JSON
		Double pLat = object.getDouble("latitude"); //get latitude from JSON
		Double pLon = object.getDouble("longitude"); //get longitude from JSON
		this.point = new LatLonPoint(pLat, pLon); // Create point
		this.serverImageUrl = object.getString("photopath");
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public double getLat(){
		return this.point.getLat();
	}
	
	public double getLon(){
		return this.point.getLon();
	}

	
	public String toJSONString(){
		String buffer = "\""+this.name+"\":{";
		buffer += "\"name\":\""+this.name+"\",";
		buffer += "\"latitude\":\""+point.getLat()+"\",";
		buffer += "\"longitude\":\""+point.getLon()+"\",";
		buffer += "\"photo\":\""+this.image+"\"";
		buffer += "}";
		
		return buffer;
	}


	public JSONObject toJSONObject(){ // Use toJSONString for now.
		
		JSONObject json = new JSONObject();
		
		try {
			json.put("name", this.name);
			json.put("latitude", this.point.getLat());
			json.put("longitude", this.point.getLon());
			//json.put("photo", this.photo);
		}
		catch (Exception e){
			Log.e("error", e.getMessage());
		}
		
		return json;
		
		
	}
	
	private void writeTest() {	
    	try
    	{
    		FileOutputStream fos = this.context.openFileOutput("MyFileName", Context.MODE_PRIVATE);
    		fos.write("Hello, World!".getBytes());
    		fos.close();
    		
    		
    		Context context = this.context.getApplicationContext();
    		CharSequence text = "File written";
    		int duration = Toast.LENGTH_SHORT;
    		Toast toast = Toast.makeText(context, text, duration);
    		toast.show();
    		
    	}
    	catch (Exception e)
    	{    		
    		Context context = this.context.getApplicationContext();
    		CharSequence text = "File not written.";
    		int duration = Toast.LENGTH_SHORT;
    		Toast toast = Toast.makeText(context, text, duration);
    		toast.show();
    	}
    }
    
    private void readTest(){
    	try {
    	    BufferedReader inputReader = new BufferedReader(new InputStreamReader(
    	            this.context.openFileInput("MyFileName")));
    	    String inputString;
    	    StringBuffer stringBuffer = new StringBuffer();                
    	    while ((inputString = inputReader.readLine()) != null) {
    	        stringBuffer.append(inputString + "\n");
    	    }
    	    String text = stringBuffer.toString();
    	    
    	    Context context = this.context.getApplicationContext();
    		int duration = Toast.LENGTH_SHORT;
    		Toast toast = Toast.makeText(context, text, duration);
    		toast.show();
    	    
    	} catch (IOException e) {
    	    e.printStackTrace();
    	}
    }

	public boolean hasImage() {
		if(image == null){
			return false;
		} else {
			return true;
		}
	}
	
	public Bitmap getServerImage(){
		try {
			URL newurl = new URL(this.serverImageUrl); 
			Bitmap bitmap = BitmapFactory.decodeStream(newurl.openConnection() .getInputStream());
			return bitmap;
		} catch(Exception e){
			return null;
		}
	}
	
	public String getServerImageUrl(){
		return this.serverImageUrl;
	}

	public String getImagePath() {
		return image.getPath();
	}

	public Map<String, String> getMap() {
		Map<String, String> properties = new HashMap<String, String>();
		
		properties.put("name", this.name);
		properties.put("latitude", Double.toString(this.getLat()));
		properties.put("longitude", Double.toString(this.getLon()));
		
		return properties;
	}


	
}
