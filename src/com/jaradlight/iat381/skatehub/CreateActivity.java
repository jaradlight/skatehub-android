package com.jaradlight.iat381.skatehub;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.jaradlight.iat381.skatehub.R;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class CreateActivity extends Activity implements OnClickListener, LocationListener {
	
	private ActionBar actionBar;
	
	private Button buttonCapturePhoto;	
	private EditText placeName;
	private ImageView photoView;
	
	private final String JPEG_FILE_PREFIX = "skatespot";
	private final String JPEG_FILE_SUFFIX = "skatespot";
		
	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	
	private double latitude;
	private double longitude;	
	private LatLonPoint point;
	
	private LocationManager locationManager;
	
	SkatehubRestClientUsage client;
	
	File currentFile;
	String mCurrentPhotoPath;
	
	private final LocationListener locationListener = new LocationListener() {
	    public void onLocationChanged(Location location) {
	        longitude = location.getLongitude();
	        latitude = location.getLatitude();
	    }

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			longitude = location.getLongitude();
			latitude = location.getLatitude();
		}
		
		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create);
		
		// Setup Action Bar
		actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		// Create RestClient for later HTTP requests.
		client = new SkatehubRestClientUsage();
		
		// Set up name edit text
		this.placeName = (EditText)findViewById(R.id.editTextPlaceName);
		//this.buttonCapturePhoto = (Button)findViewById(R.id.buttonCapturePhoto);
		//this.buttonCapturePhoto.setOnClickListener(this);
		
		// Set Up LocationManager
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		// Register the LocationListener to receive location updates.
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 15000, 10, locationListener);
		
		// setup photoView
				photoView = (ImageView) findViewById(R.id.photoView);
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	}

	public void onClickCapturePhoto(View v)
	{	// When we press the button to take a photo:

		// Update the location
		this.point = getLocation();
		
		// Make the Camera intent
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		
		// Give the intent a file to write the image to
		File imageFile = makeImageFile();
		//takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
		takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageFile);
		
		// Start the Camera intent
		startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
	}
	
	public void onClickSave(MenuItem item) // Must use MenuItem because that is the clicked item
	{	// When we press the button so save the new place:
		Log.d("msg","Save button on action bar pressed.");
		savePlace();
	}
	
	public void onClickFeedItem(View v){ 
		String message = "";
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
	}
	
	public boolean savePlace(){
		
		if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
			// GPS IS OFF!
			Toast.makeText(getApplicationContext(), "GPS is not enabled. Please enable GPS to save this location.", Toast.LENGTH_SHORT).show();
			return false;
		}
		
		Log.d("test","Save Place clicked.");
		
		Place place = generatePlace();
		Log.d("savedata","NAME :"+place.getName());
		Log.d("savedata","LAT :"+Double.toString(place.getLat()));
		Log.d("savedata","LON :"+Double.toString(place.getLon()));
		Log.d("savedata","IMAGE :"+currentFile.getPath());
		
		
		
		String name = placeName.getText().toString();
		if(name.matches("")){
			Toast.makeText(getApplicationContext(), "Please enter a title.", Toast.LENGTH_SHORT).show();
		} else {
			// do the thing.
			
			client.postNewPlace(place);
			Log.d("feed","Starting upload.");
			Toast.makeText(getApplicationContext(), "Uploading...", Toast.LENGTH_SHORT).show();
			
			finish();
//			Place p = generatePlace();
//			if(p != null){
//				addPlaceToFeed(generatePlace());
//			} else {
//				Toast.makeText(getApplicationContext(), "Error saving to feed.", Toast.LENGTH_SHORT).show();
//			}
		}
		
		
		
		return true;
	}

	
	private File makeImageFile(){
		File photoDirectory = Environment.getExternalStoragePublicDirectory(
	            Environment.DIRECTORY_PICTURES
	            + "/skatespot"
	        );
			
		Log.d("filedir", photoDirectory.getPath());
		
		// Make the required directories
		photoDirectory.mkdirs();
		
		String fileName = "skatespot_"+ getDateStamp() +".jpg";
		File outputFile = new File(photoDirectory, fileName);
		
		currentFile = outputFile;

		String string = "";

		try {
			
			FileOutputStream fos = new FileOutputStream(outputFile.getAbsolutePath());
			fos.write(string.getBytes());
			fos.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		File f = new File(fileName);
		return f;
	}
	

	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		
		Bundle extras = intent.getExtras();
		
		
		if(resultCode == RESULT_OK)
		{	// If we got data back:
			
			Bitmap bmp = (Bitmap) extras.get("data");
			photoView.setImageBitmap(bmp);
			
			// Save the file to the folder directory.
			saveImage(bmp, currentFile);
		} else {
			// No data 
			Toast.makeText(getApplicationContext(), "Photo capture cancelled.", Toast.LENGTH_SHORT).show();
		}
		
		//String.path path = extras.get(')
		
	}
	
	private void saveImage(Bitmap bmp, File file){
		try {
			FileOutputStream fos = new FileOutputStream(file);
			bmp.compress(Bitmap.CompressFormat.JPEG, 90, fos);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	private LatLonPoint getLocation(){
		// Gets the device's coordinates and returns
		// a LatLonPoint Object with the data.

		return new LatLonPoint(latitude, longitude);
	}
	

	
	public Place generatePlace(){
		// Makes a Place object from the current state.
		// TODO: add photo functionality
		
		String name = this.placeName.getText().toString();
		LatLonPoint point = getLocation();
		
		// Check values are okay.
		// Not sure if this is the best way. Probably not.
		if(name.matches("")){
			return null;
		} else if (point == null){
			return null;
		}
		
		return new Place(getApplicationContext(), name, point, currentFile);
		
	}
	
	private String getDateStamp(){
		// returns a string related to current time,
		// for file names and stuff.
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		Date now = new Date();
		return formatter.format(now);
	}
	
	private void addPlaceToFeed(Place place){
		// TODO
		// This should take a place object, open the feed
		// activity, and add it to the top of the PlaceList/
		
		Intent i = new Intent(this, FeedActivity.class);
		i.putExtra("name", place.getName());
		i.putExtra("latitude", place.getLat());
		i.putExtra("longitude", place.getLon());
		i.putExtra("imagePath", currentFile.getPath());
    	startActivity(i);
		
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

}
