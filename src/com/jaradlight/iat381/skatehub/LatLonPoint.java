package com.jaradlight.iat381.skatehub;

public class LatLonPoint {
	
	// Simple geocoords class;
	
	double latitude;
	double longitude;
	
	public LatLonPoint(double lat, double lon){
		this.latitude = lat;
		this.longitude = lon;
	}
	
	public double getLat(){
		return this.latitude;
	}
	
	public double getLon(){
		return this.longitude;
	}
}
