package com.jaradlight.iat381.skatehub;


import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.loopj.android.image.SmartImageView;

public class SingleActivity extends Activity {
	
	private GoogleMap googleMap;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single);
        
        //Toast.makeText(getApplicationContext(), "In SingleActivity!", Toast.LENGTH_SHORT).show();
        
        ImageViewTouch img = (ImageViewTouch) findViewById(R.id.map);

		Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.loc_map);
		img.setImageBitmap(bitmap);
		img.setScaleType(ScaleType.CENTER_CROP);


		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		
		ActionBar ab = getActionBar();
		ab.setTitle(bundle.getString("name"));
		//ab.setSubtitle("A place"); 
		
		
		SmartImageView singleViewImage = (SmartImageView) findViewById(R.id.singleViewImage);
		singleViewImage.setImageUrl(bundle.getString("imagePath"));
		
		
		setupItems(bundle);
		
		//setupMapView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.single, menu);
		return true;
	}
	
	private void setupItems(Bundle bundle){
		
		// Setup the name
		TextView name = (TextView) findViewById(R.id.textViewName);
		name.setText(bundle.getString("name"));
		
		//Setup the coords text string
		TextView coords = (TextView) findViewById(R.id.textViewCoords);
		String s = 	"Lat: " +
					Double.toString(bundle.getDouble("latitude")) +
					" Lon: " +
					Double.toString(bundle.getDouble("longitude"));
		coords.setText(s);
	}
	
//	private void setupMapView(){
//		// Get a handle to the Map Fragment
//        GoogleMap map = ((MapFragment) getFragmentManager()
//                .findFragmentById(R.id.singleMap)).getMap();
//
//        LatLng sydney = new LatLng(-33.867, 151.206);
//
//        map.setMyLocationEnabled(true);
//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13));
//
//        map.addMarker(new MarkerOptions()
//                .title("Sydney")
//                .snippet("The most populous city in Australia.")
//                .position(sydney));
//	}
	
//    /**
//     * function to load map. If map is not created it will create it for you
//     * */
//    private void initilizeMap() {
//        if (googleMap == null) {
//            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
//                    R.id.map)).getMap();
// 
//            // check if map is created successfully or not
//            if (googleMap == null) {
//                Toast.makeText(getApplicationContext(),
//                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
//                        .show();
//            }
//        }
//    }
//    
//    @Override
//    protected void onResume() {
//        super.onResume();
//        initilizeMap();
//    }

}
